#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>

#include <QTimer>
#include <QPushButton>

#include "modelfield.h"
#include "viewfield.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void draw_field();

    ViewField vField;
    ModelField mField;

private slots:
    void step();
    void generateButton_click();
    void clearButton_click();
    void startButton_click();
    void stopButton_click();

private:
    Ui::MainWindow *ui;
    QGraphicsScene* mainScene;
    QTimer* mainTimer;
    QPushButton* generateButton;
    QPushButton* clearButton;
    QPushButton* startButton;
    QPushButton* stopButton;
    QPushButton* nextButton;

};


#endif // MAINWINDOW_H
