#-------------------------------------------------
#
# Project created by QtCreator 2017-04-30T13:18:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CGOL
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    cell.cpp \
    modelfield.cpp \
    viewfield.cpp

HEADERS  += mainwindow.h \
    cell.h \
    modelfield.h \
    viewfield.h

FORMS    += mainwindow.ui
