#ifndef CELL_H
#define CELL_H

#define BLACK 0
#define GREEN 1

#define SIZEOFSELL 5

#include <QMainWindow>
#include <QObject>
#include <QWidget>

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QGraphicsRectItem>

class Cell : public QGraphicsRectItem {
public:
    Cell(int x, int y);

    void change_color(int newColor);

    int x;
    int y;

    QColor blackColor;
    QColor greenColor;
    QColor whiteColor;

    // QGraphicsItem interface
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
};

#endif // CELL_H
