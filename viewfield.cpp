#include "viewfield.h"

ViewField::ViewField() {
    viewField = new Cell**[FIELD_LENGHT];
    for (int i = 0; i < FIELD_LENGHT; i++) {
        viewField[i] = new Cell*[FIELD_WIDTH];
    }

    for (int i = 0; i < FIELD_LENGHT; i++) {
        for (int k = 0; k < FIELD_WIDTH; k++) {
            viewField[i][k] = new Cell(i * 5, k * 5);
        }
    }
}

Cell** ViewField::operator[](int x) {
    return viewField[x];
}

void ViewField::change_color(ModelField modelField) {
    for (int i = 0; i < FIELD_LENGHT; i++) {
        for (int k = 0; k < FIELD_WIDTH; k++) {
            if (modelField[i][k] == false) {
                viewField[i][k]->change_color(BLACK);
            } else if (modelField[i][k] == true) {
                viewField[i][k]->change_color(GREEN);
            }
        }
    }
}
