#ifndef VIEWFIELD_H
#define VIEWFIELD_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>

#include "cell.h"
#include "modelfield.h"

class ViewField {
public:
    ViewField();

    Cell** operator[](int x);

    Cell*** viewField;

    void change_color(ModelField modelField);
};

#endif // VIEWFIELD_H
