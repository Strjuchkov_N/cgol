#include "cell.h"

Cell::Cell(int x, int y): QGraphicsRectItem(0){
    setRect(0, 0, SIZEOFSELL, SIZEOFSELL);
    setPos(x, y);
    blackColor = QColor(0, 0, 0);
    greenColor = QColor(0, 255, 0);
    whiteColor = QColor(255, 255, 255);
    setBrush(blackColor);
    this->x = x;
    this->y = y;
}

void Cell::change_color(int newColor) {
    if (newColor == BLACK) setBrush(blackColor);
    if (newColor == GREEN) setBrush(greenColor);
}

void Cell::mousePressEvent(QGraphicsSceneMouseEvent *event) {
//    mField.change_cell_state(this->x, this->y);
}
