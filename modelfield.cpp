#include "modelfield.h"

ModelField::ModelField() {
    modelField = new bool*[FIELD_LENGHT];
    supportField = new bool*[FIELD_LENGHT];
    for (int i = 0; i < FIELD_LENGHT; i++) {
        modelField[i] = new bool[FIELD_WIDTH];
        supportField[i] = new bool[FIELD_WIDTH];
    }

    for (int i = 0; i < FIELD_LENGHT; i++) {
        for (int k = 0; k < FIELD_WIDTH; k++) {
            modelField[i][k] = false;
            supportField[i][k] = false;
        }
    }
}

void ModelField::rand_filling_field() {
    int randNum = 0;
    int factorOfRand = 5;
    for (int i = 0; i < FIELD_LENGHT; i++) {
        for (int k = 0; k < FIELD_WIDTH; k++) {
            randNum = rand() % factorOfRand;
            if (randNum == 0) modelField[i][k] = true;
                else modelField[i][k] = false;
        }
    }
}

void ModelField::clear_field() {
    for (int i = 0; i < FIELD_LENGHT; i++) {
        for (int k = 0; k < FIELD_WIDTH; k++) {
            modelField[i][k] = false;
        }
    }
}

void ModelField::change_cell_state(int x, int y) {
    modelField[x][y] = !(modelField[x][y]);
}

//void ModelField::save_Field() {
//    bool** lastField;
//    for (int i = 0; i < FIELD_LENGHT; i++) {
//        for (int k = 0; k < FIELD_WIDTH; k++) {
//            lastField[i][k] = modelField[i][k];
//        }
//    }

//    backUp.push_back(lastField);
//    if (backUp.size() > BACKUP_MAX_SIZE) {
//        backUp.erase(backUp.begin());
//    }

//}

int ModelField::check_area(int x, int y) {
    int detectedAlveCells = 0;
    x += FIELD_LENGHT;
    y += FIELD_WIDTH;
    for (int i = -1; i <= 1; i++) {
        for (int j = -1; j <= 1; j++) {
            if (!((i == 0) && (j == 0))) {
                if (modelField[(x + i) % FIELD_LENGHT][(y + j) % FIELD_WIDTH] == true) detectedAlveCells++;
            }
        }
    }
    return detectedAlveCells;
}

void ModelField::check_changes(int x, int y) {
    bool isCellAlive = modelField[x][y];
    int numOfAliveCellsAround = check_area(x, y);

    if (isCellAlive == false) {
        if (numOfAliveCellsAround ==  3)
            supportField[x][y] = true;
            else
                supportField[x][y] = false;
    }

    if (isCellAlive == true) {
        if (numOfAliveCellsAround < 2 || numOfAliveCellsAround > 3)
            supportField[x][y] = false;
            else
                supportField[x][y] = true;
    }

}

void ModelField::next_step() {
//    save_Field();
    for (int i = 0; i < FIELD_LENGHT; i++) {
        for (int k = 0; k < FIELD_WIDTH; k++) {
            check_changes(i, k);
        }
    }
    for (int i = 0; i < FIELD_LENGHT; i++) {
        for (int k = 0; k < FIELD_WIDTH; k++) {
            modelField[i][k] = supportField[i][k];
        }
    }
}

//void ModelField::last_step() {
//    if (!backUp.empty()) {
//        for (int i = 0; i < FIELD_LENGHT; i++) {
//            for (int k = 0; k < FIELD_WIDTH; k++) {
//                modelField[i][k] = backUp.back()[i][k];
//            }
//        }
//        backUp.pop_back();
//    }

//}

bool* ModelField::operator[](int x) {
    return modelField[x];
}
