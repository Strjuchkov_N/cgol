#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "cell.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    mainScene = new QGraphicsScene(0, 0, 500, 500, this);
    ui->graphicsView->setScene(mainScene);

    generateButton = new QPushButton("Random Filling", this);
    generateButton->setGeometry(QRect(QPoint(10, 530), QSize(141, 31)));
    connect(generateButton, SIGNAL(pressed()),
            this, SLOT(generateButton_click()));

    clearButton = new QPushButton("Clear", this);
    clearButton->setGeometry(QRect(QPoint(450, 600), QSize(50, 50)));
    connect(clearButton, SIGNAL(pressed()),
            this, SLOT(clearButton_click()));

    startButton = new QPushButton("Start", this);
    startButton->setGeometry(QRect(QPoint(10, 570), QSize(141, 31)));
    connect(startButton, SIGNAL(pressed()),
            this, SLOT(startButton_click()));

    stopButton = new QPushButton("Stop", this);
    stopButton->setGeometry(QRect(QPoint(10, 610), QSize(141, 31)));
    connect(stopButton, SIGNAL(pressed()),
            this, SLOT(stopButton_click()));

    nextButton = new QPushButton("Next Step", this);
    nextButton->setGeometry(QRect(QPoint(360, 530), QSize(141, 31)));
    connect(nextButton, SIGNAL(pressed()),
            this, SLOT(step()));

    draw_field();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::draw_field(){
    for (int i = 0; i < FIELD_LENGHT; i++) {
        for (int k = 0; k < FIELD_WIDTH; k++) {
            mainScene->addItem(vField[i][k]);
        }
    }
}

void MainWindow::step() {
    mField.next_step();
    vField.change_color(mField);
}

void MainWindow::generateButton_click() {
    mField.rand_filling_field();
    vField.change_color(mField);
}

void MainWindow::clearButton_click() {
    mField.clear_field();
    vField.change_color(mField);
}

void MainWindow::startButton_click() {
    mainTimer = new QTimer(this);
    connect(mainTimer, SIGNAL(timeout()),
            this, SLOT(step()));
    mainTimer->setInterval(300);
    mainTimer->start();

    generateButton->setEnabled(false);
}

void MainWindow::stopButton_click() {
    mainTimer->stop();

    generateButton->setEnabled(true);
}


