#ifndef MODELFIELD_H
#define MODELFIELD_H

#define FIELD_LENGHT 100
#define FIELD_WIDTH 100

#define BACKUP_MAX_SIZE 10

#include <QMainWindow>
#include <QObject>
#include <QWidget>

class ModelField {
public:
    ModelField();

    bool* operator[](int x);

    void rand_filling_field();

    void clear_field();

    void change_cell_state(int x, int y);

    void next_step();

//    void last_step();

private:
    int check_area(int x, int y);

    void check_changes(int x, int y);

//    void save_Field();

    bool** modelField;
    bool** supportField;

//    std::vector<int> backUp;
};

#endif // MODELFIELD_H
